from multiprocessing import Process, Lock ,Value
from time import sleep
from keithley2600_interface import *
import influxdb_client
from influxdb_client.client.write_api import SYNCHRONOUS
import sys
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QMainWindow, QWidget, QLabel, QLineEdit
from PyQt5.QtWidgets import QPushButton, QRadioButton, QAbstractButton, QCheckBox
from PyQt5.QtCore import QSize 
import serial.tools.list_ports
from functools import partial


channel_map={
    "HMC8042":2,
    "HMP4040":4,
    "HAMEG HO720 - HAMEG HO720":4,
    "KORAD USB Mode":1,
    "USB2.0-Ser!":1
}
rm = ResourceManager()

ports = serial.tools.list_ports.comports()
i = 1
portnames = []
device_names=[]
#address_HV = ''
print('List of Ports:')
for port, desc, hwid in sorted(ports):
    ports_line = "{}: {} [{}] \n".format(port, desc, hwid)
    print(i, ports_line)
    i += 1
    portnames.append(port)
    device_names.append(desc)

while True:
    try:    
        port_number = int(input('To which port is your HV device connected? Enter a number!\n'))
        break
    except:
        print("You have to input a number! To Exit enter 0")
if port_number==0:
    sys.exit()

port=portnames[port_number-1]
device=device_names[port_number-1]

address=f"ASRL{port}::INSTR"

'''
while True:
    try:
        port_number_HV = int(input('If you additionally want to connect a HV supply for sweeping, enter its port number. If not: Enter 100.'))
        if port_number_HV == 100:
            break
        else:
            port_HV = portnames[port_number_HV-1]
            device_HV = device_names[port_number_HV-1]
            address_HV = f'ASRL{port_HV}::INSTR'
            break
    except:
        print("You have to input a number! To Exit enter 0")
        
if port_number_HV==0:
    sys.exit()

hv = import_keithley()
'''
hw=Keithley2600(address,rm,"my_name",max_channels=channel_map[device])

token = "YYSxPMLtfQj11Y35eUm-gjklUFLTTGzmnMrjy5wyNVKBbDeERCMrAdR2clD0I9_ZAvGpRRnvbCUMI6P3OkeTuQ=="
org = "lhcb"
bucket = "power_supply_data"
# Store the URL of your InfluxDB instance
url="http://localhost:8086"  

client = influxdb_client.InfluxDBClient(
    url=url,
    token=token,
    org=org
)

# Write script
write_api = client.write_api(write_options=SYNCHRONOUS)

sleep_time = Value('d',1.0)
measurement_enable = Value('d', 0)


class input_gui(QMainWindow):
    def __init__(self, parent=None):
        super(input_gui, self).__init__(parent)
        
    # Initialising gui window
        QMainWindow.__init__(self)
        self.setMinimumSize(QSize(650, 650))   
        #self.instrument = ResourceManager.open_resource(address) 
        IDN = rm.open_resource(address).query("*IDN?")
        self.setWindowTitle(f"Power Supply Control {IDN}")
        self.selected_channel=0

    # Label for On/Off
        self.nameLabel5 = QLabel(self)
        self.nameLabel5.setText('On/Off:')
        self.nameLabel5.move(20, 1)
        self.nameLabel5.resize(50, 32)
        
    # VOLTAGE
        self.voltage_button_list={}
        self.voltage_line_list={}
        for ichan in range(1,channel_map[device]+1):
            # Label for voltage input line
            self.nameLabel = QLabel(self)
            self.nameLabel.setText('Voltage:')
            self.nameLabel.move(int(2.5*40*(ichan-1)+40+(200*(ichan-1))), 180)

            # Button to set voltage
            if ichan not in self.voltage_button_list:
                self.voltage_button_list[ichan] = QPushButton('Set SourceVoltage', self)
            self.voltage_button_list[ichan].resize(200,32)
            self.voltage_button_list[ichan].move(100*(ichan - 1) + 100+(200*(ichan-1)), 220)
            self.voltage_button_list[ichan].clicked.connect(partial(self.press_volt_button,ichan))
            self.voltage_button_list[ichan].setAutoDefault(True)

            # Voltage input line
            self.voltage_line_list[ichan] = QLineEdit(self)
            self.voltage_line_list[ichan].move((ichan-1)* 100 +100+(200*(ichan-1)), 180)
            self.voltage_line_list[ichan].resize(200, 32)
            self.voltage_line_list[ichan].returnPressed.connect(self.voltage_button_list[ichan].click)
        
    # CURRENT    
        self.current_button_list={}
        self.current_line_list={}
        for ichan in range(1,channel_map[device]+1):
            # Label for channel (is actually not a part of 'current')
            self.nameLabel4 = QLabel(self)
            self.nameLabel4.setText(f'Channel {ichan}:')
            self.nameLabel4.move(int(2.5*40*(ichan-1)+160+(200*(ichan-1))), 120)

            # Label for current input line
            self.nameLabel2 = QLabel(self)
            self.nameLabel2.setText('Current:')
            self.nameLabel2.move(int(2.5*40*(ichan-1)+40+(200*(ichan-1))), 270)
            
            # Button to set current
            if ichan not in self.current_button_list:
                self.current_button_list[ichan] = QPushButton('Set CurrentLimit', self)
            self.current_button_list[ichan].clicked.connect(partial(self.press_curr_button, ichan))
            self.current_button_list[ichan].setAutoDefault(True)
            self.current_button_list[ichan].resize(200,32)
            self.current_button_list[ichan].move((ichan-1)* 100 +100+(200*(ichan-1)), 310)   

            # Current input line
            self.current_line_list[ichan] = QLineEdit(self)
            self.current_line_list[ichan].move((ichan-1)* 100 +100+(200*(ichan-1)), 270)
            self.current_line_list[ichan].resize(200, 32)
            self.current_line_list[ichan].returnPressed.connect(self.current_button_list[ichan].click)

    # Sleep time
        for ichan in range(1,channel_map[device]+1):
        # Label for sleep time input line
            self.nameLabel3 = QLabel(self)
            self.nameLabel3.setText('Sleep time:')
            self.nameLabel3.move(32, 360)
            
            # Button to set sleep time
            sleep_button = QPushButton('Set sleep time', self)
            sleep_button.clicked.connect(self.press_sleep_button)
            sleep_button.resize(200,32)
            sleep_button.move(100, 400)     

            # Sleep time input line
            self.line3 = QLineEdit(self)
            self.line3.move(100, 360)
            self.line3.resize(200, 32)
            self.line3.returnPressed.connect(sleep_button.click)
            
    # Sweep
            #address_HV = address
            self.sweep_button_list={}
            self.sweep_stop_line_list={}
            self.sweep_step_line_list={}
            self.sweep_start_line_list={}
            self.sweep_sleep_line_list={}
            
            self.nameLabel9 = QLabel(self)
            self.nameLabel9.setText('Sweeping (with channel 2 only):')
            self.nameLabel9.resize(300,20)
            self.nameLabel9.move(170, 515)
            
            k = 2
            # Label for sweep start input line
            self.nameLabel6 = QLabel(self)
            self.nameLabel6.setText('V_Start:')
            self.nameLabel6.move(95+(k-1)*290, 480)

            # Label for sweep stop input line
            self.nameLabel7 = QLabel(self)
            self.nameLabel7.setText('V_Stop:')
            self.nameLabel7.move(155+(k-1)*290, 480)

            # Label for sweep step input line
            self.nameLabel7 = QLabel(self)
            self.nameLabel7.setText('Step:')
            self.nameLabel7.move(210+(k-1)*290, 480)

            # Label for sweep sleeptime input line
            self.nameLabel8 = QLabel(self)
            self.nameLabel8.setText('Sleep:')
            self.nameLabel8.move(260+(k-1)*290, 480)

            # Button to activate sweep
            if k not in self.sweep_button_list:
                self.sweep_button_list[k] = QPushButton(f'SweepChannel {k}', self)
            self.sweep_button_list[k].clicked.connect(partial(self.press_sweep_button, k))
            self.sweep_button_list[k].resize(200,32)
            self.sweep_button_list[k].move(100+(k-1)*290, 550)   

            # Sweep start input
            self.sweep_start_line_list[k] = QLineEdit(self)
            self.sweep_start_line_list[k].move(100+(k-1)*290, 510)
            self.sweep_start_line_list[k].resize(50, 32)
            self.sweep_start_line_list[k].returnPressed.connect(self.sweep_button_list[k].click)

            # Sweep stop input
            self.sweep_stop_line_list[k] = QLineEdit(self)
            self.sweep_stop_line_list[k].move(150+(k-1)*290, 510)
            self.sweep_stop_line_list[k].resize(50, 32)
            self.sweep_stop_line_list[k].returnPressed.connect(self.sweep_button_list[k].click)

            # Sweep step input
            self.sweep_step_line_list[k] = QLineEdit(self)
            self.sweep_step_line_list[k].move(200+(k-1)*290, 510)
            self.sweep_step_line_list[k].resize(50, 32)
            self.sweep_step_line_list[k].returnPressed.connect(self.sweep_button_list[k].click)

            # Sweep sleeptime input
            self.sweep_sleep_line_list[k] = QLineEdit(self)
            self.sweep_sleep_line_list[k].move(250+(k-1)*290, 510)
            self.sweep_sleep_line_list[k].resize(50, 32)
            self.sweep_sleep_line_list[k].returnPressed.connect(self.sweep_button_list[k].click)
            
    # Channel/Master activation

        # Button to activate Channel
        
        self.channel_button_list={}
        for ichan in range(1,channel_map[device]+1):
            if ichan not in self.channel_button_list:
               self.channel_button_list[ichan]= QCheckBox(f'Channel {ichan}', self)
            self.channel_button_list[ichan].clicked.connect(self.press_channel_button)
            self.channel_button_list[ichan].move(ichan*100, 1)
            #hw.select_channel(ichan)
            if int(hw.status_channel()) == 1:
                self.channel_button_list[ichan].setChecked(True) 
            else:
                self.channel_button_list[ichan].setChecked(False)
          
        
        '''   
        # Button to activate Master
        master_button = QCheckBox('Master', self)
        master_button.clicked.connect(self.press_master_button)
        master_button.move(100, 1)
        if int(hw.status_master()) == 1:
            master_button.setChecked(True)
        else:
            master_button.setChecked(False)
        '''
    # Start/stop measurement button 

        # Start measurement button
        start_measurement_button = QRadioButton('Start Measurement', self)
        start_measurement_button.move(100, 50)
        start_measurement_button.resize(200,32)
        start_measurement_button.clicked.connect(self.press_start_measurement_button)
        
        # Stop measurement button
        stop_measurement_button = QRadioButton('Stop Measurement', self)
        stop_measurement_button.move(300, 50)
        stop_measurement_button.resize(200,32)
        stop_measurement_button.clicked.connect(self.press_stop_measurement_button)
        
    # Functions of buttons

    def press_volt_button(self,channel):
        voltage = self.voltage_line_list[channel].text()
        lock.acquire()
        if hw.select_channel(channel):
            hw.adjust_voltage(voltage)
        lock.release()    
    
    def press_curr_button(self, channel):
        current = self.current_line_list[channel].text()
        lock.acquire()
        if hw.select_channel(channel):
            hw.adjust_current(current)
        lock.release()

    def press_sleep_button(self):
        try:
            sleep_time.value = float(self.line3.text())
        except:
            print('Enter a Number.')
    
    def press_sweep_button(self, channel):
        try:
            start = float(self.sweep_start_line_list[channel].text())
            stop = float(self.sweep_stop_line_list[channel].text())
            step = float(self.sweep_step_line_list[channel].text())
            sleeptime = float(self.sweep_sleep_line_list[channel].text())
            Process(target=loop_c,args=(lock,start,stop, step, sleeptime, channel, address)).start()
        except:
            print('Irregular input. You have to enter a number (int or float)!')
    def press_channel_button(self):
        for channel in self.channel_button_list:
            #hw.select_channel(channel)
            lock.acquire()
            status = int(hw.status_channel())
            if self.channel_button_list[channel].isChecked() != status:
                if status == 0: 
                    hw.activate_channel(1)
                else:
                    hw.activate_channel(0)
            lock.release()

    def press_master_button(self):
        status = int(hw.status_master())
        if status == 0:
            hw.activate_master(1)
        else:
            hw.activate_master(0)

    def press_start_measurement_button(self):
        measurement_enable.value = 1
        Process(target=loop_a,args=(lock,)).start()
        
    def press_stop_measurement_button(self):
        measurement_enable.value = 0
    
# Process loops  

def loop_a(lock):
    
    error_count1 = 0
    try:
        while (measurement_enable.value == 1):

            for ichan in range(1,channel_map[device]+1):
                lock.acquire()
                #hw.select_channel(ichan)
                hmc_voltage = float(hw.read_voltage())
                hmc_current = float(hw.read_current())
                lock.release()
                print("Writing channel ",ichan," to InfluxDB!\n", "voltage:",hmc_voltage,"current:", hmc_current,"sleep time:",sleep_time.value)
                p = influxdb_client.Point("keithley2600_voltage").tag("device",f"keithley2600_ch{ichan}").field("voltage", hmc_voltage)
                q = influxdb_client.Point("keithley2600_current").tag("device",f"keithley2600_ch{ichan}").field("current", hmc_current)
                write_api.write(bucket=bucket, org=org, record=p)
                write_api.write(bucket=bucket, org=org, record=q)
            sleep(sleep_time.value)

    except:
        print(f'An error has occured. Error_count: {error_count1}')
        error_count1 += 1
        if error_count1 < 100:
            pass
        
    else:
        print('Measurement stopped!')

def loop_b(lock):
    app = QtWidgets.QApplication(sys.argv)
    mainWin = input_gui()
    mainWin.show()
    sys.exit( app.exec_() )

def loop_c(lock, start, stop, step, sleeptime, channel, address):
    hw.sweep(start, stop, step, sleeptime, channel, address)
    #address_HV = address_HV

if __name__ == '__main__':
    lock = Lock()
    Process(target=loop_b,args=(lock,)).start()
