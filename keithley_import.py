from pyvisa import ResourceManager
from time import sleep
import influxdb_client
from influxdb_client.client.write_api import SYNCHRONOUS
import numpy as np
from multiprocessing import Lock

RM = ResourceManager()

token = "YYSxPMLtfQj11Y35eUm-gjklUFLTTGzmnMrjy5wyNVKBbDeERCMrAdR2clD0I9_ZAvGpRRnvbCUMI6P3OkeTuQ=="
org = "lhcb"
bucket = "power_supply_data"
# Store the URL of your InfluxDB instance
url="http://localhost:8086"

client = influxdb_client.InfluxDBClient(
    url=url,
    token=token,
    org=org
)
lock = Lock()
# Write script
write_api = client.write_api(write_options=SYNCHRONOUS)

class import_keithley:

    def sweep(self, start, stop, step, sleeptime, channel, address_HV):
        address_HV = address_HV
        if channel == 1:
            channel = 'a'
        if channel == 2:
            channel = 'b'
        self.inst = RM.open_resource(address_HV)
        self.inst.write(f'smu{channel}.source.output = smu{channel}.OUTPUT_ON')
        if start < stop:
            for n in np.arange(start, stop, step):
                lock.acquire()
                self.inst.write(f'smu{channel}.source.levelv = {n}')
                self.inst.write(f'print(smu{channel}.measure.v())')
                v = float(self.inst.read())
                lock.release()
                p = influxdb_client.Point("HV_supply_voltage").tag("device",f"Keithley2600_ch{channel}").field("voltage", v)
                write_api.write(bucket=bucket, org=org, record=p)
                lock.acquire()
                self.inst.write(f'print(smu{channel}.measure.i())')
                i = float(self.inst.read())
                lock.release()
                q = influxdb_client.Point("HV_supply_current").tag("device",f"Keithley2600_ch{channel}").field("current", i)
                write_api.write(bucket=bucket, org=org, record=q)
                print(i,v)
                sleep(sleeptime)
        else:
            for n in np.arange(start, stop, -step):
                lock.acquire()
                self.inst.write(f'smu{channel}.source.levelv = {n}')
                self.inst.write(f'print(smu{channel}.measure.v())')
                v = float(self.inst.read())
                lock.release()
                p = influxdb_client.Point("HV_supply_voltage").tag("device",f"Keithley2600_ch{channel}").field("voltage", v)
                write_api.write(bucket=bucket, org=org, record=p)
                lock.acquire()
                self.inst.write(f'print(smu{channel}.measure.i())')
                i = float(self.inst.read())
                lock.release()
                q = influxdb_client.Point("HV_supply_current").tag("device",f"Keithley2600_ch{channel}").field("current", i)
                write_api.write(bucket=bucket, org=org, record=q)
                print(i,v)
                sleep(sleeptime)
        self.inst.write(f'smu{channel}.source.output = smu{channel}.OUTPUT_OFF')
